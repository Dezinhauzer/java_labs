package com.company.chain.of.responsibility.interfaces;

import com.company.factory.method.interfaces.Vehicle;

public interface ChainOfResponsibility {
    void setNext(ChainOfResponsibility nextChain);

    void handle(Vehicle vehicle, String fileName);
}
