package com.company.chain.of.responsibility.models;

import com.company.adapter.OutputStreamAdapter;
import com.company.factory.method.VehicleHelper;
import com.company.factory.method.interfaces.Vehicle;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

public class PrintRowHandler extends DefaultHandler {

    public PrintRowHandler() {
    }

    @Override
    public void handle(Vehicle vehicle, String fileName) {
        if (vehicle.getSize() > 3) {
            this.nextHandler.handle(vehicle, fileName);
            return;
        }

        try (PrintStream printStream = new PrintStream(fileName)) {
            printStream.println(VehicleHelper.toRowString(vehicle));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
