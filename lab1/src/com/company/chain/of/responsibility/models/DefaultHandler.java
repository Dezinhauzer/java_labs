package com.company.chain.of.responsibility.models;

import com.company.chain.of.responsibility.interfaces.ChainOfResponsibility;
import jdk.internal.dynalink.ChainedCallSite;

public abstract class DefaultHandler implements ChainOfResponsibility {
    protected ChainOfResponsibility nextHandler;

    @Override
    public void setNext(ChainOfResponsibility nextChain) {
        this.nextHandler = nextChain;
    }

}
