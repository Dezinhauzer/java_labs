package com.company.chain.of.responsibility.models;

import com.company.factory.method.VehicleHelper;
import com.company.factory.method.interfaces.Vehicle;

import java.io.IOException;
import java.io.PrintStream;

public class PrintColumnHandler extends DefaultHandler {

    public PrintColumnHandler() {

    }

    @Override
    public void handle(Vehicle vehicle,String fileName) {
        if (vehicle.getSize() <= 3) {
            nextHandler.handle(vehicle,fileName);
            return;
        }

        try (PrintStream printStream = new PrintStream(fileName)) {
            printStream.println(VehicleHelper.toColumnString(vehicle));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
