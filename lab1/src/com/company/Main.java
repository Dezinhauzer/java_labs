package com.company;

import com.company.adapter.InputStreamAdapter;
import com.company.chain.of.responsibility.models.PrintColumnHandler;
import com.company.chain.of.responsibility.models.PrintRowHandler;
import com.company.command.interfaces.CommandPrint;
import com.company.command.models.PrintColumnCommandPrint;
import com.company.command.models.PrintRowCommandPrint;
import com.company.factory.method.VehicleHelper;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.exceptions.ModelPriceOutOfBoundsException;
import com.company.factory.method.exceptions.NoSuchModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.factory.method.interfaces.VehicleFactory;
import com.company.factory.method.models.Car;
import com.company.factory.method.models.MotorCycle;
import com.company.visitor.models.PrintVisitor;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;


public class Main {

    public static void main(String[] args) throws Exception {
    /*    Properties properties = ConfigReader.getUniqueInstance().getProperties();

        System.out.printf("It's property: \nname: %s\ngroup: %s\n", properties.getProperty("name"), properties.getProperty("group"));

        println("\nЗадание для машины");
        Vehicle car = new Car("Мой бренд", 3);
        printDefaultTask(car);

        println("\nЗадание для мотоцикла");
        Vehicle motocycle = new MotorCycle("Бренд мотоцикла", 3);
        printDefaultTask(motocycle);

        println("Демонстрация статического класса");

        println("Демонстрация класса автомобиль");
        printStaticClass(car);

        println("Демонстрация класса мотоцикл");
        printStaticClass(motocycle);

        println("Демонстрация фабрики");
        MotoCycleFactory motoCycleFactory = new MotoCycleFactory();
        CarFactory carFactory = new CarFactory();

        println("Фабрика машины");
        Car carInstance = (Car) printFactoryMethod(carFactory);

        println("Фабрика мотоцикл");
        MotorCycle motorCycleInstance = (MotorCycle) printFactoryMethod(motoCycleFactory);

        println("Демонстрируем прототип");
        println("Демонстрация прототипа машины");
        prototypePrintMethod(carInstance);

        println("Демонстрация прототипа мотоцикла");
        prototypePrintMethod(motorCycleInstance);*/
        /**
         * 2 Лаба
         */
/*
        Car carInstance = new Car("Мой бренд", 5);

        println("Адаптер");
        String[] strings = {"Hi", "it", "is", "an Adapter."};
        String test = "test";

        try (OutputStreamAdapter outputStreamAdapter = new OutputStreamAdapter(new FileOutputStream(test))) {
            outputStreamAdapter.write(strings);
        }

        try (InputStreamAdapter inputStreamAdapter = new InputStreamAdapter(new FileInputStream(test))) {
            Arrays.stream(inputStreamAdapter.readString()).forEach(System.out::println);
        }

        println("Декоратор");
        Vehicle decorator = VehicleHelper.synchronizedVehicle(carInstance);

        new Thread(() -> {
            println("Поток 1");
            VehicleHelper.printNames(decorator);
        }).start();
        new Thread(() -> {
            println("Поток 2");
            VehicleHelper.printNames(decorator);
        }).start();*/

        /**
         * Лаба 3
         */

        println("Цепочка зависимостей");

        PrintColumnHandler printColumnHandler = new PrintColumnHandler();
        PrintRowHandler printRowHandler = new PrintRowHandler();
        printColumnHandler.setNext(printRowHandler);

        Car car = new Car("Car", 3);
        MotorCycle motorCycle = new MotorCycle("MotorCycle", 4);
        printColumnHandler.handle(car, "carOutput.txt");
        printColumnHandler.handle(motorCycle, "motorCycle.txt");

        println("Команды");

        PrintColumnCommandPrint printColumnCommandPrint = new PrintColumnCommandPrint();
        PrintRowCommandPrint printRowCommandPrint = new PrintRowCommandPrint();
        String rowStringFileName = "rowCommand.txt";
        String columnStringFileName = "columnCommand.txt";

        println("Команда вывода в столбик");
        commandPrintMethod(car, printColumnCommandPrint, rowStringFileName);

        println("Команда вывода в строку");
        commandPrintMethod(car, printRowCommandPrint, columnStringFileName);

        println("Итератор");
        Iterator iterator = car.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().toString());
        }

        println("Моменто");
        Car.Momento momento = car.createMomento();

        println("Изменяем");
        car.setBrand("Я новая модель");
        car.setModelNameByModelName("Модель номер: 2", "Я после момента");
        carIterator(car);

        println("Получили момент обратно");
        car.setMomento(momento);
        carIterator(car);

        println("Визитор");
        Arrays.asList(new Vehicle[]{
                new Car("car", 3),
                new MotorCycle("MotorCycle", 3)
        }).forEach(el -> el.accept(new PrintVisitor()));
    }

    private static void carIterator(Car car) {
        System.out.printf("Модель: %s%n", car.getBrand());
        ((Iterator) car.iterator())
                .forEachRemaining(System.out::println);
    }

    private static void commandPrintMethod(Car car, CommandPrint printColumnCommandPrint, String rowStringFileName) throws Exception {
        try (Writer outputStream = new FileWriter(rowStringFileName)) {
            car.setCommandPrint(printColumnCommandPrint);
            car.print(outputStream);
        }

        try (InputStreamAdapter inputStreamAdapter = new InputStreamAdapter(new FileInputStream(rowStringFileName))) {
            Arrays.stream(inputStreamAdapter.readString())
                    .forEach(System.out::println);
        }
    }

    public static void prototypePrintMethod(Vehicle vehicle) throws CloneNotSupportedException, DuplicateModelNameException, NoSuchModelNameException {
        Vehicle clone = vehicle.clone();
        clone.setBrand("Бренд клон");
        clone.setModelNameByModelName("Модель номер: 3", "Я твой клон");
        clone.removeModelByModelName("Модель номер: 4");

        println("Исходный объект");
        VehicleHelper.printNames(vehicle);

        println("Склонированный Объект с измененым брендом и именем одной из моделей");
        VehicleHelper.printNames(clone);
    }

    public static Vehicle printFactoryMethod(VehicleFactory vehicleFactory) throws DuplicateModelNameException {
        VehicleHelper.setFactory(vehicleFactory);

        Vehicle vehicle = VehicleHelper.createInstance("Бренд фабрики", 5);
        VehicleHelper.printNames(vehicle);
        VehicleHelper.printPrices(vehicle);

        return vehicle;
    }

    public static void printStaticClass(Vehicle vehicle) {
        println("Распечатка всех моделей");
        VehicleHelper.printNames(vehicle);

        println("Распечатка всех цен");
        VehicleHelper.printPrices(vehicle);

        println("Среднее арифметическое");
        System.out.printf("Среднее арифметическое: %s\n", VehicleHelper.getAveragePrice(vehicle));
    }

    public static void printDefaultTask(Vehicle vehicle) throws DuplicateModelNameException, NoSuchModelNameException {
        println("Получение бренда");
        System.out.printf("Марка автомобился: %s\n", vehicle.getBrand());

        println("Установка нового бренда");
        vehicle.setBrand("Новый бренд");
        System.out.printf("Марка автомобился: %s\n", vehicle.getBrand());

        println("Проверка всех моделей");
        VehicleHelper.printNames(vehicle);

        println("Установка нового имени для модели");
        String newModelName = "Новая модель";
        vehicle.setModelNameByModelName("Модель номер: 1", newModelName);
        VehicleHelper.printNames(vehicle);

        println("Устнановка новой цены для модели");
        System.out.printf("Цена модели %s: %s\n", newModelName, vehicle.getPriceByModelName(newModelName));
        vehicle.setPriceByModelName(newModelName, 12412);
        System.out.printf("Цена модели %s: %s\n", newModelName, vehicle.getPriceByModelName(newModelName));

        println("Проверка всех печати всех цен");
        VehicleHelper.printPrices(vehicle);

        println("Добавление новой модели");
        String addModelName = "Добавленная модель";
        double addModelPrice = 123;
        vehicle.addNewModel(addModelName, addModelPrice);
        VehicleHelper.printNames(vehicle);

        println("Удаление добавленной модели");
        vehicle.removeModelByModelName(addModelName);
        VehicleHelper.printNames(vehicle);

        println("Исключение нет модели");
        try {
            vehicle.getPriceByModelName("test");
        } catch (NoSuchModelNameException exception) {
            System.out.println(exception.getMessage());
        }

        println("Исключение модель с таким именем уже существует");
        try {
            vehicle.setModelNameByModelName(newModelName, newModelName);
        } catch (DuplicateModelNameException exception) {
            System.out.println(exception.getMessage());
        }

        println("Исключение неверной цены");
        try {
            vehicle.setPriceByModelName(newModelName, -123123);
        } catch (ModelPriceOutOfBoundsException exception) {
            exception.printStackTrace(System.out);
        }
    }

    private static void println(String text) {
        System.out.println("\n" + text);
    }
}