package com.company.command.interfaces;

import com.company.factory.method.models.Car;

import java.io.OutputStream;
import java.io.Writer;

public interface CommandPrint {
    void executeCommand(Car car, Writer outputStream);
}
