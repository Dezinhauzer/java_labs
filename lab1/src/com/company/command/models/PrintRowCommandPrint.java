package com.company.command.models;

import com.company.command.interfaces.CommandPrint;
import com.company.factory.method.VehicleHelper;
import com.company.factory.method.models.Car;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

public class PrintRowCommandPrint implements CommandPrint, Serializable {
    @Override
    public void executeCommand(Car car, Writer outputStream) {
        try {
            outputStream.write(VehicleHelper.toRowString(car));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}
