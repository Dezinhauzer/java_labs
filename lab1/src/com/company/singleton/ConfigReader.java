package com.company.singleton;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    private static ConfigReader uniqueInstance;
    private final Properties properties;

    public static synchronized ConfigReader getUniqueInstance() throws IOException {
        if (uniqueInstance == null) {
            synchronized (ConfigReader.class) {
                if (uniqueInstance == null) {
                    uniqueInstance = new ConfigReader();
                }
            }
        }

        return uniqueInstance;
    }

    public Properties getProperties() {
        return properties;
    }

    private ConfigReader() throws IOException {
        properties = new Properties();

        readConfigProperties();
    }

    private void readConfigProperties() throws IOException {
        String propertyName = "config.properties";
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyName)) {
            if (inputStream == null) {
                throw new FileNotFoundException(String.format("property file %s not found un the classpath", propertyName));
            }

            properties.load(inputStream);
        }
    }
}
