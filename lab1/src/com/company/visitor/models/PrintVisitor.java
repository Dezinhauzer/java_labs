package com.company.visitor.models;

import com.company.factory.method.VehicleHelper;
import com.company.factory.method.interfaces.Vehicle;
import com.company.factory.method.models.Car;
import com.company.factory.method.models.MotorCycle;
import com.company.visitor.interfaces.Visitor;

public class PrintVisitor implements Visitor {

    @Override
    public void visitCar(Car car) {
        System.out.println(VehicleHelper.toRowString(car));
    }

    @Override
    public void visitMotorcycle(MotorCycle motorCycle) {
        System.out.println(VehicleHelper.toColumnString(motorCycle));
    }
}
