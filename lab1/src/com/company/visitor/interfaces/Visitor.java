package com.company.visitor.interfaces;

import com.company.factory.method.models.Car;
import com.company.factory.method.models.MotorCycle;

public interface Visitor {
    void visitCar(Car car);
    void visitMotorcycle(MotorCycle motorCycle);
}
