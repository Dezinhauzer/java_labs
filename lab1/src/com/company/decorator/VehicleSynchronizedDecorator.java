package com.company.decorator;

import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.exceptions.NoSuchModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.visitor.interfaces.Visitor;

public class VehicleSynchronizedDecorator implements Vehicle {

    private final Vehicle vehicle;

    public VehicleSynchronizedDecorator(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public synchronized String getBrand() {
        return vehicle.getBrand();
    }

    @Override
    public synchronized void setBrand(String brand) {
        vehicle.setBrand(brand);
    }

    @Override
    public synchronized String[] getModelsName() {
        /**
         * Показываем что у нас всё синхронизируется
         */
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return vehicle.getModelsName();
    }

    @Override
    public synchronized void setModelNameByModelName(String findModelName, String newModelName) throws NoSuchModelNameException, DuplicateModelNameException {
        vehicle.setModelNameByModelName(findModelName, newModelName);
    }

    @Override
    public synchronized double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        return vehicle.getPriceByModelName(modelName);
    }

    @Override
    public synchronized void setPriceByModelName(String modelName, double newPrice) throws NoSuchModelNameException {
        vehicle.setPriceByModelName(modelName, newPrice);
    }

    @Override
    public synchronized double[] getArrayPriceModel() {
        return vehicle.getArrayPriceModel();
    }

    @Override
    public synchronized void addNewModel(String modelName, double price) throws DuplicateModelNameException {
        vehicle.addNewModel(modelName, price);
    }

    @Override
    public synchronized void removeModelByModelName(String modelName) throws NoSuchModelNameException {
        vehicle.removeModelByModelName(modelName);
    }

    @Override
    public synchronized Vehicle clone() throws CloneNotSupportedException {
        return vehicle.clone();
    }

    @Override
    public synchronized int getSize() {
        return vehicle.getSize();
    }

    @Override
    public synchronized void accept(Visitor visitor) {
        vehicle.accept(visitor);
    }
}
