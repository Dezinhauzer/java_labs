package com.company.factory.method.exceptions;

public class NoSuchModelNameException extends Exception {
    public NoSuchModelNameException(String modelName) {
        super(String.format("Модель с именем %s !Е", modelName));
    }
}
