package com.company.factory.method;

import com.company.decorator.VehicleSynchronizedDecorator;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.interfaces.VehicleFactory;
import com.company.factory.method.interfaces.Vehicle;
import com.company.factory.method.models.CarFactory;
import com.sun.istack.internal.NotNull;
import interfaces.DAOFactory;
import models.TextDAO;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class VehicleHelper {
    private static VehicleFactory factory = new CarFactory();
    private static DAOFactory daoFactory = new TextDAO();

    public static void setFactory(@NotNull VehicleFactory newFactory) {
        factory = newFactory;
    }

    public static void setDaoFactory(@NotNull DAOFactory newDaoFactory) {
        daoFactory = newDaoFactory;
    }

    public static void initialization(Vehicle vehicle, int countElement) throws DuplicateModelNameException {
        for (int i = 0; i < countElement; i++) {
            vehicle.addNewModel(String.format("Модель номер: %d", i + 1), (i + 1) * 1000);
        }
    }

    public static double getAveragePrice(Vehicle vehicle) {
        double summa = 0;
        double[] prices = vehicle.getArrayPriceModel();
        for (double price : prices) {
            summa += price;
        }

        return summa / (double) vehicle.getSize();
    }

    public static void printNames(Vehicle vehicle) {
        System.out.printf("Список моделей в марке: %s%n", vehicle.getBrand());
        for (String name : vehicle.getModelsName()) {
            System.out.println(name);
        }
    }

    public static void printPrices(Vehicle vehicle) {
        System.out.printf("Список цен в марке: %s%n", vehicle.getBrand());
        for (double price : vehicle.getArrayPriceModel()) {
            System.out.println(price);
        }
    }

    public static Vehicle createInstance(String nameModel, int countModel) throws DuplicateModelNameException {
        return factory.createInstance(nameModel, countModel);
    }

    public static Vehicle synchronizedVehicle(Vehicle vehicle) {
        return new VehicleSynchronizedDecorator(vehicle);
    }

    public static String toRowString(Vehicle vehicle) {
        return getStringVehicleWithSeparator(vehicle, ";");
    }

    public static String toColumnString(Vehicle vehicle) {
        return getStringVehicleWithSeparator(vehicle, "%n");
    }

    private static String getStringVehicleWithSeparator(Vehicle vehicle, String rowSeparator) {
        StringBuilder stringBuilder = new StringBuilder(
                String.format("Бренд: %s" + rowSeparator + "Количество моделей: %s" + rowSeparator, vehicle.getBrand(), vehicle.getSize())
        );

        String[] models = vehicle.getModelsName();
        double[] prices = vehicle.getArrayPriceModel();
        for (int i = 0; i < models.length; i++) {
            stringBuilder.append(
                    String.format("Модель: %s, Цена: %s" + rowSeparator, models[i], prices[i])
            );
        }

        return stringBuilder.toString();
    }

    public static Vehicle getByBrandName(String brandName) throws Exception {
        return daoFactory.getByBrandName(brandName);
    }

    public static void removeByBrandName(String brandName) throws Exception {
        daoFactory.remove(brandName);
    }

    public static void save(Vehicle vehicle) throws IOException {
        daoFactory.add(vehicle);
    }

    public static List<Vehicle> getAll() throws IOException {
        return daoFactory.getAll();
    }

    public static String modelListToString(Vehicle vehicle) {
        String[] modelNames = vehicle.getModelsName();
        double[] prices = vehicle.getArrayPriceModel();
        int size = vehicle.getSize();
        return IntStream.range(0, size)
                .mapToObj(index ->
                        String.format("brandName:%s,price:%s,", modelNames[size - 1 - index], prices[size - 1 - index])
                )
                .reduce((value, accum) -> accum += value).get();
    }
}
