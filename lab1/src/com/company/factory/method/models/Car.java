package com.company.factory.method.models;

import com.company.command.interfaces.CommandPrint;
import com.company.factory.method.VehicleHelper;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.exceptions.ModelPriceOutOfBoundsException;
import com.company.factory.method.exceptions.NoSuchModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.visitor.interfaces.Visitor;

import java.io.*;
import java.util.Arrays;
import java.util.Iterator;

public class Car implements Vehicle, Serializable {
    private String brand;
    private int size;
    private Model[] modelList;
    private CommandPrint commandPrint;

    public void setCommandPrint(CommandPrint commandPrint) {
        this.commandPrint = commandPrint;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Car(String brandName, int countModel) throws DuplicateModelNameException {
        brand = brandName;
        modelList = new Model[0];

        VehicleHelper.initialization(this, countModel);
    }

    @Override
    public String[] getModelsName() {
        String[] names = new String[modelList.length];
        for (int i = 0; i < modelList.length; i++) {
            names[i] = modelList[i].getName();
        }

        return names;
    }

    @Override
    public void setModelNameByModelName(String findModelName, String newModelName) throws NoSuchModelNameException, DuplicateModelNameException {
        if (getIndexModelByName(newModelName) != -1) {
            throw new DuplicateModelNameException(newModelName);
        }

        int indexModel = getIndexModelByName(findModelName);
        if (indexModel == -1) {
            throw new NoSuchModelNameException(findModelName);
        }

        modelList[indexModel].setName(newModelName);
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        int indexModel = getIndexModelByName(modelName);

        if (indexModel == -1) {
            throw new NoSuchModelNameException(modelName);
        }

        return modelList[indexModel].getPrice();
    }

    @Override
    public void setPriceByModelName(String modelName, double newPrice) throws NoSuchModelNameException {
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }

        int indexModel = getIndexModelByName(modelName);

        if (indexModel == -1) {
            throw new NoSuchModelNameException(modelName);
        }

        modelList[indexModel].setPrice(newPrice);
    }

    @Override
    public double[] getArrayPriceModel() {
        double[] prices = new double[modelList.length];
        for (int i = 0; i < modelList.length; i++) {
            prices[i] = modelList[i].price;
        }

        return prices;
    }

    @Override
    public void addNewModel(String modelName, double price) throws DuplicateModelNameException {
        int indexModel = getIndexModelByName(modelName);

        if (indexModel != -1) {
            throw new DuplicateModelNameException(modelName);
        }

        modelList = Arrays.copyOf(modelList, modelList.length + 1);
        modelList[modelList.length - 1] = new Model(modelName, price);
        size++;
    }

    @Override
    public void removeModelByModelName(String modelName) throws NoSuchModelNameException {
        int indexModel = getIndexModelByName(modelName);

        if (indexModel == -1) {
            throw new NoSuchModelNameException(modelName);
        }

        Model[] newModelList = new Model[modelList.length - 1];

        System.arraycopy(modelList, 0, newModelList, 0, indexModel);
        System.arraycopy(modelList, indexModel + 1, newModelList, indexModel, modelList.length - indexModel - 1);

        modelList = Arrays.copyOf(newModelList, newModelList.length);
        size--;
    }

    @Override
    public Vehicle clone() throws CloneNotSupportedException {
        Car clone = (Car) super.clone();
        clone.modelList = clone.modelList.clone();
        for (int i = 0; i < clone.modelList.length; i++) {
            clone.modelList[i] = clone.modelList[i].clone();
        }

        return clone;
    }

    @Override
    public int getSize() {
        return this.size;
    }

    public void print(Writer outputStream) throws Exception {
        if (commandPrint == null) {
            throw new Exception("Command not found");
        }

        commandPrint.executeCommand(this, outputStream);
    }

    public CarIterator iterator() {
        return new CarIterator();
    }

    public Momento createMomento() {
        return new Momento(this);
    }

    public void setMomento(Momento momento) throws Exception {
        momento.getCar(this);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitCar(this);
    }

    private int getIndexModelByName(String modelName) {
        int indexModel = 0;
        while (indexModel < modelList.length && !modelList[indexModel].name.equals(modelName)) {
            indexModel++;
        }

        return indexModel == modelList.length
                ? -1
                : indexModel;
    }

    private void setNewCar(Car readObject) {
        this.brand = readObject.brand;
        this.modelList = readObject.modelList;
    }

    protected static class Model implements Cloneable, Serializable {
        private String name;
        private double price;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public Model(String name, double price) {
            this.name = name;
            this.price = price;
        }

        @Override
        public Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }

        @Override
        public String toString() {
            return String.format("Модель: %s, Цена: %s", name, price);
        }
    }

    private class CarIterator implements Iterator<Model> {
        private int currentIndex = 0;

        @Override
        public boolean hasNext() {
            return currentIndex < size;
        }

        @Override
        public Model next() {
            return modelList[currentIndex++];
        }
    }

    public static class Momento {
        private byte[] carBytes;

        public Momento(Car car) {
            setCar(car);
        }

        public void setCar(Car car) {
            try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
                try (ObjectOutputStream dataOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
                    dataOutputStream.writeObject(car);
                    dataOutputStream.flush();
                }

                byteArrayOutputStream.flush();
                carBytes = byteArrayOutputStream.toByteArray();
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        public void getCar(Car newCar) throws Exception {
            if (carBytes == null) {
                throw new Exception("Момента не существует");
            }

            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(carBytes)) {
                try (ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {
                    newCar.setNewCar((Car) objectInputStream.readObject());
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }
}
