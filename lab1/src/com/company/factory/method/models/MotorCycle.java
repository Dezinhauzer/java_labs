package com.company.factory.method.models;

import com.company.factory.method.VehicleHelper;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.exceptions.ModelPriceOutOfBoundsException;
import com.company.factory.method.exceptions.NoSuchModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.visitor.interfaces.Visitor;
import com.sun.org.apache.xpath.internal.operations.Mod;

public class MotorCycle implements Vehicle {

    private Model head = new Model();

    private String brand;
    private int size;

    {
        head.prev = head;
        head.next = head;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public void setBrand(String newBrand) {
        brand = newBrand;
    }

    public MotorCycle(String brandName, int countElement) throws DuplicateModelNameException {
        brand = brandName;
        size = 0;

        VehicleHelper.initialization(this, countElement);
    }

    @Override
    public String[] getModelsName() {
        String[] names = new String[size];
        Model first = head.getNext();

        int i = 0;
        while (first != head) {
            names[i++] = first.getNameModel();
            first = first.getNext();
        }

        return names;
    }

    @Override
    public void setModelNameByModelName(String findModelName, String newModelName) throws NoSuchModelNameException, DuplicateModelNameException {
        if (getModelByName(newModelName) != null) {
            throw new DuplicateModelNameException(newModelName);
        }

        Model findModel = getModelByName(findModelName);
        if (findModel == null) {
            throw new NoSuchModelNameException(findModelName);
        }

        findModel.setNameModel(newModelName);
    }

    @Override
    public double getPriceByModelName(String modelName) throws NoSuchModelNameException {
        Model findModel = getModelByName(modelName);
        if (findModel == null) {
            throw new NoSuchModelNameException(modelName);
        }

        return findModel.getPrice();
    }

    @Override
    public void setPriceByModelName(String modelName, double newPrice) throws NoSuchModelNameException {
        if (newPrice < 0) {
            throw new ModelPriceOutOfBoundsException();
        }

        Model findModel = getModelByName(modelName);
        if (findModel == null) {
            throw new NoSuchModelNameException(modelName);
        }

        findModel.setPrice(newPrice);
    }

    @Override
    public double[] getArrayPriceModel() {
        double[] prices = new double[size];
        Model first = head.getNext();
        int i = 0;

        while (first != head) {
            prices[i++] = first.getPrice();
            first = first.getNext();
        }

        return prices;
    }

    @Override
    public void addNewModel(String modelName, double price) throws DuplicateModelNameException {
        if (getModelByName(modelName) != null) {
            throw new DuplicateModelNameException(modelName);
        }

        Model model = new Model(modelName, price);
        model.setNext(head);
        model.setPrev(head.prev);
        head.getPrev().setNext(model);
        head.setPrev(model);

        size += 1;
    }

    @Override
    public void removeModelByModelName(String modelName) throws NoSuchModelNameException {
        Model model = getModelByName(modelName);
        if (model == null) {
            throw new NoSuchModelNameException(modelName);
        }

        model.getPrev().setNext(model.getNext());
        model.getNext().setPrev(model.getPrev());

        size -= 1;
    }

    @Override
    public Vehicle clone() throws CloneNotSupportedException {
        MotorCycle clone = (MotorCycle) super.clone();
        Model cloneHead = new Model();
        clone.head = cloneHead;
        clone.head.setNext(clone.head);
        clone.head.setPrev(clone.head);

        Model originalElement = this.head.getNext();

        while (originalElement != this.head) {
            Model newModel = originalElement.clone();

            newModel.setPrev(cloneHead.getPrev());
            newModel.setNext(cloneHead);

            cloneHead.getPrev().setNext(newModel);
            cloneHead.setPrev(newModel);

            originalElement = originalElement.getNext();
        }

        return clone;
    }

    @Override
    public void accept(Visitor visitor){
        visitor.visitMotorcycle(this);
    }

    private Model getModelByName(String modelName) {
        Model first = head.getNext();

        while (first != head && !first.getNameModel().equals(modelName)) {
            first = first.getNext();
        }

        return first == head
                ? null
                : first;
    }

    private class Model implements Cloneable {
        private String nameModel = null;
        private double price = Double.NaN;
        private Model prev = null;
        private Model next = null;

        public String getNameModel() {
            return nameModel;
        }

        public void setNameModel(String nameModel) {
            this.nameModel = nameModel;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public Model getPrev() {
            return prev;
        }

        public void setPrev(Model prev) {
            this.prev = prev;
        }

        public Model getNext() {
            return next;
        }

        public void setNext(Model next) {
            this.next = next;
        }

        public Model() {
        }

        public Model(String nameModel, double price) {
            this.nameModel = nameModel;
            this.price = price;
        }

        @Override
        public Model clone() throws CloneNotSupportedException {
            return (Model) super.clone();
        }
    }

    @Override
    public int getSize() {
        return this.size;
    }
}
