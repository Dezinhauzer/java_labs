package com.company.factory.method.models;

import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.interfaces.VehicleFactory;
import com.company.factory.method.interfaces.Vehicle;

public class CarFactory implements VehicleFactory {

    @Override
    public Vehicle createInstance(String modelName, int countModel) throws DuplicateModelNameException {
        return new Car(modelName, countModel);
    }
}
