package com.company.factory.method.models;

import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.factory.method.interfaces.VehicleFactory;

public class MotoCycleFactory implements VehicleFactory {

    @Override
    public Vehicle createInstance(String modelName, int countModel) throws DuplicateModelNameException {
        return new MotorCycle(modelName, countModel);
    }
}
