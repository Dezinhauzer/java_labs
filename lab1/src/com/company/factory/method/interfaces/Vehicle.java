package com.company.factory.method.interfaces;

import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.exceptions.NoSuchModelNameException;
import com.company.visitor.interfaces.Visitor;

public interface Vehicle extends Cloneable {
    String getBrand();

    void setBrand(String brand);

    String[] getModelsName();

    void setModelNameByModelName(String findModelName, String newModelName) throws NoSuchModelNameException, DuplicateModelNameException;

    double getPriceByModelName(String modelName) throws NoSuchModelNameException;

    void setPriceByModelName(String modelName, double newPrice) throws NoSuchModelNameException;

    double[] getArrayPriceModel();

    void addNewModel(String modelName, double price) throws DuplicateModelNameException;

    void removeModelByModelName(String modelName) throws NoSuchModelNameException;

    Vehicle clone() throws CloneNotSupportedException;

    int getSize();

    String toString();

    void accept(Visitor visitor);
}
