package com.company.factory.method.interfaces;

import com.company.factory.method.exceptions.DuplicateModelNameException;

public interface VehicleFactory {
    Vehicle createInstance(String modelName, int countModel) throws DuplicateModelNameException;
}
