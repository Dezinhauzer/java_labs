import models.SaxStrategy;
import models.StrategyContext;

public class Main {

    public static void main(String[] args) throws Exception {

        if (args.length != 2) {
            System.out.println("Неверное число параметров");
            return;
        }

        StrategyContext strategyManager = new StrategyContext();
        //strategyManager.executeStrategy(Main.class.getClassLoader().getResource(args[0]).getPath(), args[1]);

        strategyManager.setStudentCheckStrategy(new SaxStrategy());
        strategyManager.executeStrategy(Main.class.getClassLoader().getResource(args[0]).getPath(), args[1]);
    }
}