package interfaces;

public interface StudentCheckStrategy {
    void checkAverageStudent(String inputNameFile, String outputNameFile) throws Exception;
}
