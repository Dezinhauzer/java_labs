package models;

import interfaces.StudentCheckStrategy;

public class StrategyContext {
    private StudentCheckStrategy studentCheckStrategy = new DomStrategy();

    public void setStudentCheckStrategy(StudentCheckStrategy studentCheckStrategy) {
        this.studentCheckStrategy = studentCheckStrategy;
    }

    public void executeStrategy(String inputFileName, String outputFileName) throws Exception {
        studentCheckStrategy.checkAverageStudent(inputFileName, outputFileName);
    }
}
