package models;

public class Subject {
    private String title;
    private int mark;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) throws Exception {
        if (mark > 5 || mark < 1) {
            throw new Exception("Оценка меньше 1 или больше 5");
        }

        this.mark = mark;
    }

    public Subject() {
    }

    public Subject(String title, int mark) {
        this.title = title;
        this.mark = mark;
    }
}
