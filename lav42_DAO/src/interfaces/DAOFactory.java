package interfaces;

import com.company.factory.method.interfaces.Vehicle;

import java.io.IOException;
import java.util.List;

public interface DAOFactory {
    void add(Vehicle vehicle) throws IOException;

    Vehicle getByBrandName(String brandName) throws Exception;

    void remove(String brandName) throws Exception;

    List<Vehicle> getAll() throws IOException;
}
