package interfaces;

import com.company.factory.method.interfaces.Vehicle;

import java.io.IOException;
import java.util.List;

public interface FileService {
    void saveAll(List<Vehicle> vehicle) throws IOException;

    List<Vehicle> getAll() throws IOException;
}
