package models;

import com.company.factory.method.interfaces.Vehicle;
import interfaces.DAOFactory;
import interfaces.FileService;
import services.DaoUtils;
import services.FileSerializableUtils;

import java.io.IOException;
import java.util.List;

public class AbstractDAO implements DAOFactory {
    protected String fileName;
    protected FileService fileService;

    @Override
    public void add(Vehicle vehicle) throws IOException {
        DaoUtils.add(vehicle, fileService);
    }

    @Override
    public Vehicle getByBrandName(String brandName) throws Exception {
        return DaoUtils.getByBrandName(brandName, fileService);
    }

    @Override
    public void remove(String brandName) throws Exception {
        DaoUtils.remove(brandName, fileService);
    }

    @Override
    public List<Vehicle> getAll() throws IOException {
        return DaoUtils.getAll(fileService);
    }
}
