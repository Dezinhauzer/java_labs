package models;

import services.FileSerializableUtils;

public class SerializableDAO extends AbstractDAO {
    public SerializableDAO() {
        fileName = "SerializableDao.txt";
        fileService = new FileSerializableUtils(fileName);
    }
}
