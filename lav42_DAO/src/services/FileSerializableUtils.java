package services;

import com.company.factory.method.interfaces.Vehicle;
import interfaces.FileService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileSerializableUtils implements FileService {
    private String fileName;

    public FileSerializableUtils(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void saveAll(List<Vehicle> vehicle) throws IOException {
        File fileDao = new File(fileName);
        fileDao.createNewFile();

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileDao))) {
            objectOutputStream.writeObject(vehicle);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Vehicle> getAll() {
        ArrayList<Vehicle> vehicles = new ArrayList<>();
        File fileDao = new File(fileName);
        try {
            fileDao.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileDao))) {
            Object readObject = objectInputStream.readObject();
            if (readObject == null) {
                return vehicles;
            }

            vehicles = (ArrayList<Vehicle>) readObject;
        } catch (IOException e) {
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return vehicles;
    }
}
