package services;

import com.company.factory.method.interfaces.Vehicle;
import interfaces.FileService;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class DaoUtils {
    public static void add(Vehicle vehicle, FileService fileService) throws IOException {
        List<Vehicle> list = fileService.getAll();
        list.add(vehicle);
        fileService.saveAll(list);
    }

    public static Vehicle getByBrandName(String brandName, FileService fileService) throws Exception {
        Vehicle vehicle = fileService.getAll().stream().filter(el -> el.getBrand().equals(brandName)).findFirst().get();
        if (vehicle == null) {
            throw new Exception("Такого бренда не существует");
        }

        return vehicle;
    }

    public static void remove(String brandName, FileService fileService) throws Exception {
        List<Vehicle> vehicleList = fileService.getAll();
        Vehicle vehicle = vehicleList.stream().filter(el -> el.getBrand().equals(brandName)).findFirst().get();
        if (vehicle == null) {
            throw new Exception("Такого бренда не существует");
        }
        fileService.saveAll(vehicleList.stream()
                .filter(el -> !el.getBrand().equals(brandName)).collect(Collectors.toList())
        );
    }

    public static List<Vehicle> getAll(FileService fileService) throws IOException {
        return fileService.getAll();
    }
}
