package services;

import com.company.factory.method.VehicleHelper;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import interfaces.FileService;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class FileUtils implements FileService {
    private String fileName;

    public FileUtils(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void saveAll(List<Vehicle> vehicles) {
        File fileDao = new File(fileName);
        try {
            fileDao.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (PrintWriter writer = new PrintWriter(fileDao)) {
            vehicles.forEach(vehicle -> {
                writer.println(
                        String.format(
                                "Brand:%s;models:%s",
                                vehicle.getBrand(),
                                VehicleHelper.modelListToString(vehicle))
                );
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Vehicle> getAll() throws IOException {
        List<Vehicle> vehicles = new ArrayList<>();

        File fileDao = new File(fileName);
        fileDao.createNewFile();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileDao))) {
            String record = bufferedReader.readLine();
            while (record != null) {
                String[] splitRecord = record.split(Pattern.quote(";"));
                String[] splitBrand = splitRecord[0].split(",");
                String[] splitModels = splitRecord[1].split(",");
                splitModels[0] = splitModels[0].substring(splitModels[0].indexOf(':') + 1);


                Vehicle vehicle = VehicleHelper.createInstance(getValue(splitBrand[0]), 0);

                IntStream.range(0, splitModels.length / 2)
                        .forEach(index -> {
                            try {
                                int mainIndex = index * 2;
                                int brandIndex = mainIndex + 1;

                                vehicle.addNewModel(
                                        getValue(splitModels[mainIndex]),
                                        Double.parseDouble(getValue(splitModels[brandIndex]))
                                );
                            } catch (DuplicateModelNameException e) {
                                e.printStackTrace();
                            }
                        });

                vehicles.add(vehicle);
                record = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DuplicateModelNameException e) {
            e.printStackTrace();
        }

        return vehicles;
    }

    private String getValue(String input) {
        return input.substring(input.indexOf(":") + 1);
    }
}
