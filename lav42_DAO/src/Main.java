import com.company.factory.method.VehicleHelper;
import com.company.factory.method.exceptions.DuplicateModelNameException;
import com.company.factory.method.interfaces.Vehicle;
import com.company.factory.method.models.Car;
import models.SerializableDAO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws Exception {
        // Текстовый ДАО
        System.out.println("Текстовый Дао-------------------------------------------------------------------------------------");
        testDao();
        // Сериализация
        System.out.println("Сериализируемый Дао-------------------------------------------------------------------------------------");
        VehicleHelper.setDaoFactory(new SerializableDAO());
        testDao();
    }

    private static void testDao() throws Exception {
        IntStream.rangeClosed(0, 2)
                .forEach(index -> {
                    try {
                        VehicleHelper.save(
                                new Car(
                                        String.format("Brand %s", index), index + 1
                                )
                        );
                    } catch (DuplicateModelNameException | IOException e) {
                        e.printStackTrace();
                    }
                });

        List<Vehicle> vehicles = VehicleHelper.getAll();
        printVehicles(vehicles);

        System.out.println("---------------------Удаляем бренд 2");
        VehicleHelper.removeByBrandName("Brand 2");
        printVehicles(VehicleHelper.getAll());

        System.out.println("---------------------Печатаем бренд 1");
        System.out.println(
                VehicleHelper.toColumnString(
                        VehicleHelper.getByBrandName("Brand 1")
                )
        );
    }

    private static void printVehicles(List<Vehicle> vehicles) {
        vehicles.forEach(el -> {
            System.out.println(VehicleHelper.toColumnString(el));
        });
    }
}
