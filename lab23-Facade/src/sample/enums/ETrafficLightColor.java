package sample.enums;

public enum ETrafficLightColor {
    RED,
    YELLOW,
    GREEN
}
