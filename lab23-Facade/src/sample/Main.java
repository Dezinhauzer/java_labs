package sample;

import javafx.application.Application;
import javafx.stage.Stage;
import sample.models.Facade;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Facade facade = new Facade(primaryStage);
        facade.drawTrafficLight();
        facade.drawCar();
        facade.startAnimation();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
