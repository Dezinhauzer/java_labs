package models;

import interfaces.Observer;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import sample.service.DrawHelper;

public class Nose implements Observer {
    private Group noseGroup;
    private Circle circle;
    private double x;
    private double y;
    private double r;

    public Group getNoseGroup() {
        return noseGroup;
    }

    public Nose(double x, double y, double r, String id) {
        noseGroup = new Group();
        noseGroup.setId(id);
        this.x = x;
        this.y = y;
        this.r = r;

        drawNose();
    }

    private void drawNose() {
        circle = DrawHelper.drawCircle(x, y, r);
        circle.setFill(Color.BLUE);

        noseGroup.getChildren().add(circle);
    }

    @Override
    public void update() {
        circle.setFill(
                circle.getFill() == Color.BLUE
                        ? Color.RED
                        : Color.BLUE
        );
    }
}
