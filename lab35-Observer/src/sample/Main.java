package sample;

import javafx.application.Application;
import javafx.stage.Stage;
import models.SmileScene;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        new SmileScene(primaryStage);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
